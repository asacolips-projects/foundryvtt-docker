## Prerequisites.

First, install Docker Community Edition. See instructions for [MacOS](https://docs.docker.com/docker-for-mac/install/), [Windows](https://docs.docker.com/docker-for-windows/install/), or your preferred flavor of Linux ([CentOS, RHEL, Amazon Linux](https://docs.docker.com/install/linux/docker-ce/centos/), [Debian](https://docs.docker.com/install/linux/docker-ce/debian/), [Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/), [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)). After that, install [Docker Compose](https://docs.docker.com/compose/install/).

## Set Up

### Extract the Beta

Extract the contents of the Linux beta build (not Windows or Mac!) to your `/foundry-app` directory of this repo. If your extraction utility placed the contents into a sub-folder, make sure the contents (dist, lib, locales, node_modules, etc.) are directly inside the `/foundry-app` directory. This docker-compose configuration will only work with Foundry 0.4.0 or higher.

Systems, modules, worlds, and other persistent data are stored separately in the `/foundry-data` folder. Those can usually be installed from within Foundry's browser UI.

### Run docker-compose.yml

To start the containers, you'll need to run the following command from this repo's main directory:

```
docker-compose up -d
```

The `-d` option is for detached mode, but you can also leave the option off if you want to see log files. If you start in detached mode, you can stop the containers with `docker-compose down`.

## Updating Versions

Whenever a new version of Foundry is released, extract its contents to the `/foundry-app` directory just like before. Because the app directory is baked into the node container that we're using for Foundry, you'll want to make sure to rebuild the node container. Run the following the first time after starting the containers following an update:

```
docker-compose up -d --build
```

## Access Foundry in the browser!

To pull up Foundry, go to `http://localhost:30000`, or `http://yourdomain.com:30000` if you're using a server with a real domain.

## Installing systems

Once Foundry is running, go to `http://localhost:30000/setup`, click the "Game Systems" link, and use the "Install System" button to install a system via its manifest URL. For example, paste in the following URL for the dnd5e system: [https://gitlab.com/foundrynet/dnd5e/blob/master/system.json](https://gitlab.com/foundrynet/dnd5e/blob/master/system.json)

## Installing modules

Once Foundry is running, go to `http://localhost:30000/setup`, click the "Add-on Modules" link, and use the "Install Module" button to install a module via its manifest URL.

## Changing the port

To change the port, you'll need to update the port both in `docker-compose.yml` and `foundry-data/Config/options.json`
